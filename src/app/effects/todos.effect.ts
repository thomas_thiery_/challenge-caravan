import {Effect, Actions} from "@ngrx/effects";
import {TodosService} from './../services/todosService';

import {Injectable} from "@angular/core";

import * as effectActions from '../constants/ActionTypes';
import * as todoActions from '../actions/index';

@Injectable()
export class TodoEffects {
  constructor(private action$: Actions, private _api: TodosService) {
  }

  @Effect()
  loadTodo$ = this.action$
    .ofType(effectActions.LIST_TODO)
    .switchMap(() => this._api.loadTodos()
      .map(res => todoActions.listTodoCompleted(res['results']))
    );

  @Effect()
  addTodo$ = this.action$
    .ofType(effectActions.ADD_TODO)
    .switchMap(payload => this._api.createTodo(payload)
      .map(res => todoActions.createTodoCompleted(res))
    );

  @Effect()
  deleteTodo$ = this.action$
    .ofType(effectActions.DELETE_TODO)
    .switchMap(payload => this._api.deleteTodo(payload)
      .map(() => todoActions.deleteTodoCompleted(payload.id))
    );

  @Effect()
  updateTodo$ = this.action$
    .ofType(effectActions.EDIT_TODO)
    .switchMap(payload => this._api.editTodo(payload)
      .map(() => todoActions.editTodoCompleted(payload.todo))
    );
}
