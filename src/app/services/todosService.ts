import {Todo} from '../models/todos';
import {Injectable} from '@angular/core';
import {Headers, Http, Request, RequestMethod, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TodosService {

  API_TODOS_URL: string = `notes/`;

  constructor(private http: Http) {
  }

  private createAuthorizationHeader(headers: Headers) {
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);
  }

  loadTodos(): Observable<Todo[]> {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    // ToDo : Use a pagination for the notes

    return this.http.get(this.API_TODOS_URL, {headers: headers})
      .map((res: Response) => res.json());
  }

  createTodo(body): Observable<Todo> {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    return this.http.post(this.API_TODOS_URL, body.todo, {headers: headers})
      .map((res: Response) => res.json());
  }

  editTodo(body: any): Observable<Todo> {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    return this.http.put(`${this.API_TODOS_URL}/${body.todo.id}`, body.todo, {headers: headers})
      .map((res: Response) => res.json());
  }

  deleteTodo(body: any): Observable<Todo> {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    return this.http.delete(`${this.API_TODOS_URL}${body.id}`, {headers: headers})
      .map((res: Response) => res.json());
  }
}
