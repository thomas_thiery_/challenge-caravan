import {Injectable}     from '@angular/core';
import {CanActivate, Router}    from '@angular/router';
import {AuthService} from './tokenService';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate() {
    return this.checkLogin();
  }


  checkLogin(): boolean {
    if (this.authService.isLogged()) {
      return true;
    }

    /* Clear state on logout */
    this.router.navigate(['']);
    return false;
  }
}
