import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  constructor(private _http: Http, private _router: Router) {
  }

  login(username: string, password: string) {
    this._http.post('auth/', {username: username, password: password}).subscribe(
      response => {
        localStorage.setItem('token', JSON.parse(response['_body']).token);
        this._router.navigate(['notes']);
      },
      error => {
        console.error(error)
      },
      () => console.log('login#done')
    );
  }

  logout() {
    localStorage.removeItem('token');
    this._router.navigate(['']);
  }

  isLogged() {
    /* hack to quiclky check if there is a token */
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }
}
