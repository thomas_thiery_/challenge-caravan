import {Component} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './containers/App';
import {LoginComponent} from './containers/Login';
import {AuthGuard} from './services/authGuard';

@Component({
  selector: 'caravan-root',
  template: '<router-outlet></router-outlet>'
})
export class RootComponent {
}

export const routes: Routes = [
  {
    path: 'notes',
    component: AppComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: LoginComponent
  }
];

export const routing = RouterModule.forRoot(routes);
