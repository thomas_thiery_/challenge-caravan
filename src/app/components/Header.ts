import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import * as actions from '../actions/index';
import { AuthService } from '../services/tokenService';

@Component({
  selector: 'caravan-header',
  template: require('./Header.html'),
  providers: [AuthService]
})
export class HeaderComponent {
  constructor(public store: Store<any[]>, private _authService: AuthService) {}

  handleSave(title: string) {
    if (title.length !== 0) {
      let todo = {
        comment: 'hardcoded',
        title: title
      };
      this.store.dispatch(actions.addTodo(todo));
    }
  }

  handleClick() {
    this._authService.logout();
  }
}
