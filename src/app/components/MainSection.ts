import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Rx';
import * as actions from '../actions/index';

@Component({
  selector: 'caravan-main-section',
  template: require('./MainSection.html')
})
export class MainSectionComponent {
  todos: Observable<any>;
  filteredTodos: Observable<any>;
  completedCount: Observable<any>;

  constructor(public store: Store<any[]>) {
    this.filteredTodos = Observable.combineLatest(store.select('todos'), (todos: any) => todos);
    this.todos = store.select('todos');
  }

  ngOnInit() {
    this.store.dispatch(actions.listTodo());
  }

  handleSave(e: any) {
    if (e.title.length === 0) {
      this.store.dispatch(actions.deleteTodo(e.id));
    } else {
      e.comment = 'hardcoded';
      this.store.dispatch(actions.editTodo(e));
    }
  }

  handleDestroy(e: any) {
    this.store.dispatch(actions.deleteTodo(e));
  }
}
