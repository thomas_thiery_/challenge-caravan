import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'caravan-todo-item',
  template: require('./TodoItem.html')
})
export class TodoItemComponent {
  @Input() todo: any;
  @Output() onDestroy: EventEmitter<any> = new EventEmitter(false);
  @Output() onSave: EventEmitter<any> = new EventEmitter(false);
  editing: boolean = false;

  handleSave(title: string) {
    this.onSave.emit({id: this.todo.id, title});
    this.editing = false;
  }

  handleDoubleClick() {
    this.editing = true;
  }

  handleClick() {
    this.onDestroy.emit(this.todo.id);
  }
}
