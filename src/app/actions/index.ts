import * as types from '../constants/ActionTypes';
import {Todo} from "../models/todos";

export function deleteTodo(id: string) {
  return {type: types.DELETE_TODO, id};
}

export function deleteTodoCompleted(id: string) {
  return {type: types.DELETE_TODO_COMPLETED, id};
}

export function editTodo(todo) {
  return {type: types.EDIT_TODO, todo};
}
export function editTodoCompleted(todo) {
  return {type: types.EDIT_TODO_COMPLETED, todo};
}

export function listTodo() {
  return {type: types.LIST_TODO};
}

export function listTodoCompleted(todos: Todo[]) {
  return {type: types.LIST_TODO_COMPLETED, todos};
}

export function addTodo(todo) {
  return {type: types.ADD_TODO, todo};
}

export function createTodoCompleted(todo: Todo) {
  return {type: types.ADD_TODO_COMPLETED, todo};
}
