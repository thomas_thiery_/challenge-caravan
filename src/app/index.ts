import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {TodoEffects} from './effects/todos.effect';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {routing, RootComponent} from './routes';
import {store} from './reducers';
import {HttpModule, RequestOptions, JsonpModule} from '@angular/http';
import {TodosService} from './services/todosService';
import {AuthService} from './services/tokenService';
import {AuthGuard} from './services/authGuard';

import {AppComponent} from './containers/App';
import {LoginComponent} from './containers/Login';
import {HeaderComponent} from './components/Header';
import {MainSectionComponent} from './components/MainSection';
import {TodoItemComponent} from './components/TodoItem';
import {TodoTextInputComponent} from './components/TodoTextInput';
import {AuthModule} from 'angular2-auth';

import {CustomRequestOptions} from './services/httpCustomConfig';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    EffectsModule.run(TodoEffects),
    AuthModule.forRoot(),
    JsonpModule,
    store
  ],
  declarations: [
    RootComponent,
    AppComponent,
    LoginComponent,
    HeaderComponent,
    MainSectionComponent,
    TodoItemComponent,
    TodoTextInputComponent
  ],
  bootstrap: [RootComponent],
  providers: [{provide: RequestOptions, useClass: CustomRequestOptions}, TodosService, AuthGuard, AuthService]
})
export class AppModule {
}

