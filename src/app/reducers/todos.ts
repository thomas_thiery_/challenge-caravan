import * as reducerActions from '../constants/ActionTypes';

/* Plug effect to the todos reducers */

export const todos = (state: any[], action: any) => {

  switch (action.type) {
    case reducerActions.ADD_TODO_COMPLETED:
      return [
        {
          id: action.todo.id,
          title: action.todo.title,
          comment: action.todo.comment
        },
        ...state
      ];

    case reducerActions.LIST_TODO_COMPLETED:
      return [...action.todos];

    case reducerActions.DELETE_TODO_COMPLETED:
      return state.filter(todo =>
        todo.id !== action.id
      );

    case reducerActions.EDIT_TODO_COMPLETED:
      return state.map(todo =>
        todo.id === action.todo.id ?
          Object.assign({}, todo, {title: action.todo.title}) :
          todo
      );

    default:
      return state;
  }
};
