import {Component} from '@angular/core';
import { LoginModel }    from '../models/loginModel';
import { AuthService } from '../services/tokenService';

/**
 *
 *
 * @export
 * @class LoginComponent
 */
@Component({
  selector: 'caravan-login',
  template: require('./Login.html'),
  providers: [AuthService]
})

export class LoginComponent {
  public model: LoginModel = {'username': '', 'password': ''};

  constructor(private _authService: AuthService) {
  }

  loginClicked() {
    this._authService.login(this.model.username, this.model.password);
  }
}
