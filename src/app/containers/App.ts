import {Component} from '@angular/core';

@Component({
  selector: 'caravan-app',
  template: require('./App.html')
})
export class AppComponent {}
